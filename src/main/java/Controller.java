import java.util.Scanner;
import java.util.regex.Pattern;

public class Controller {

    /*
    * A regular expression that checks that the first character of
    * the surname is capitalized, and all the rest are small and have
    * no other characters except for the alphabet of the English and Russian alphabets
    * */
    private String regLastname = "^([А-Я]+[а-я]+)|[A-Z]+[a-z]+";

    /*
    * A regular expression that checks if the phone number starts with
    * +380 and checks if there are 9 digits, spaces or dashes after that
    * */
    private String regNumber = "\\+380((\\d{9})|((\\s|-)\\d{2}(\\s|-)\\d{3}(\\s|-)\\d{2}(\\s|-)\\d{2}))";

    /*
    * The regular expression that checks for a nickname has English letters,
    * numbers, and certain characters such as space and !@#$%^&*_+
    * */
    private String regNickname = "^[A-z0-9 !@#$%^&*_+]+$";

    private Model model;
    private View view;
    private Scanner sc;


    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        sc = new Scanner(System.in);
    }

    public void saveDate(){
        String ln = validLastName();
        String nn = validNickname();
        String num = validNumber();
        model.setLastName(ln);
        model.setNickname(nn);
        model.setNumber(num);
        view.printInfo(View.SUCCESS);
    }

    /*
    * A function that loops through input until it matches a
    * specific regular expression, in this case regLastname
    * */
    private String validLastName(){
        Pattern pattern = Pattern.compile(regLastname);
        String lastName;
        view.printInfo(View.INPUT_LAST_NAME);
        while (true){
            view.printInfo(View.EXAMPLE_LAST_NAME);
            lastName = sc.nextLine();
            if (pattern.matcher(lastName).matches()){
                break;
            }
            view.printInfo(View.ERROR_LAST_NAME, View.ERROR_VALID_VALUE);
        }
        return lastName;
    }

    /*
     * A function that loops through input until it matches a
     * specific regular expression, in this case regNickname
     * */
    private String validNickname(){
        Pattern pattern = Pattern.compile(regNickname);
        String nickname;
        view.printInfo(View.INPUT_NICKNAME);
        while (true){
            view.printInfo(View.EXAMPLE_NICKNAME);
            nickname = sc.nextLine();
            if (pattern.matcher(nickname).matches()){
                break;
            }
            view.printInfo(View.ERROR_NICKNAME, View.ERROR_VALID_VALUE);
        }
        return nickname;
    }

    /*
     * A function that loops through input until it matches a
     * specific regular expression, in this case regNumber
     * */
    private String validNumber(){
        Pattern pattern = Pattern.compile(regNumber);
        String number;
        view.printInfo(View.INPUT_NUMBER);
        while (true){
            view.printInfo(View.EXAMPLE_NUMBER);
            number = sc.nextLine();
            if (pattern.matcher(number).matches()){
                break;
            }
            view.printInfo(View.ERROR_NUMBER, View.ERROR_VALID_VALUE);
        }
        return number.replace("\\s|-","");
    }
}
