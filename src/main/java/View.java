public class View {

    public static String INPUT_LAST_NAME = "Please, input last name.";
    public static String INPUT_NICKNAME = "Please, input nickname.";
    public static String INPUT_NUMBER = "Please, input number.";
    public static String EXAMPLE_LAST_NAME = "Example last name: Ivanov or Иванов.";
    public static String EXAMPLE_NICKNAME = "Example nickname: example123.";
    public static String EXAMPLE_NUMBER = "Example number: +380631234567, +380 63 123 45 67 or +380-63-123-45-67.";
    public static String ERROR_VALID_VALUE = "Please enter a valid value.";
    public static String ERROR_LAST_NAME = "Last name was entered incorrectly.";
    public static String ERROR_NICKNAME = "Nickname entered incorrectly.";
    public static String ERROR_NUMBER = "Number entered incorrectly.";
    public static String SUCCESS = "Data was saved.";

    public void printInfo(String data){
        System.out.println(data);
    }

    public void printInfo(String ... data){
        for (String s:data) {
            System.out.println(s);
        }
    }
}
